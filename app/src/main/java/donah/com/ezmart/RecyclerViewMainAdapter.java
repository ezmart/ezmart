package donah.com.ezmart;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
/**
 * Created by acer on 1/7/2017.
 */

public class RecyclerViewMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<itemModel> data= Collections.emptyList();
    itemModel current;
    int currentPos=0;

    //private List<itemModel> stList;

    public RecyclerViewMainAdapter(List<itemModel> price) {
        this.data = price;

    }

    public static final String key_total= "total";
    // create constructor to innitilize context and data sent from MainActivity
    public RecyclerViewMainAdapter(Context context, List<itemModel> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    // Inflate the layout when viewholder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.recycler_view_list_row, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        // Get current position of item in recyclerview to bind data and assign values from list
        final MyHolder myHolder= (MyHolder) holder;
        final itemModel current=data.get(position);
        myHolder.tvName.setText(current.description);
        myHolder.retailPrice.setText("P " + current.retail);
       // myHolder.retailPrice.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));

        myHolder.records.setChecked(current.isSelected());

       final ArrayList<Float> stringArrayList = new ArrayList<Float>();
        myHolder.records.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;

                String PRICE = data.get(position).retail;

                data.get(position).setSelected(cb.isChecked());
                 /*  boolean checked=false;
                            Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: "
                                + PRICE, Toast.LENGTH_LONG).show();

                if(cb.isChecked())
                {
                    checked=true;
                    String PRICE = data.get(position).retail;
                    float fprice= Float.parseFloat(PRICE);


                    stringArrayList.add(fprice);


                }

                    //String name=friendList.get(position).get(FriendList.FRIEND_NAME);
                Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: " +  sum, Toast.LENGTH_LONG).show();*/
                //current.setSelected(cb.isChecked());


            }
        });

    }

    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<itemModel> getPrice() {
        return data;
    }

    class MyHolder extends RecyclerView.ViewHolder{

        CheckBox records;
        TextView retailPrice, tvName;

        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            records= (CheckBox) itemView.findViewById(R.id.records);
            retailPrice= (TextView) itemView.findViewById(R.id.tvPrice);
            tvName= (TextView) itemView.findViewById(R.id.tvName);

        }

    }

}
