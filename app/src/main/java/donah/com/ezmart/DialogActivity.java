package donah.com.ezmart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Created by acer on 1/28/2017.
 */

public class DialogActivity extends DialogFragment {

    LayoutInflater layoutInflater;
    View v;
    EditText search;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        layoutInflater = getActivity().getLayoutInflater();
        v = layoutInflater.inflate(R.layout.activity_search_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                search = (EditText) v.findViewById(R.id.search_item);
                Toast.makeText(v.getContext(), search.getText(), Toast.LENGTH_SHORT).show();


            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }
}
