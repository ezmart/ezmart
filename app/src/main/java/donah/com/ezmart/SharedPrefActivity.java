package donah.com.ezmart;

/**
 * Created by Donnah Grace Ongayo on 10/3/2016.
 */

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPrefActivity {
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "Ezmart";

    public static final String KEY_ID = "record_id";

    public static final String KEY_PRICE = "price";


    public SharedPrefActivity(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void checkedData(String record_id, String price) {
        // Storing name in pref
        editor.putString(KEY_ID, record_id);

        // Storing email in pref
        editor.putString(KEY_PRICE, price);


        // commit changes
        editor.commit();
    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // record_id
        user.put(KEY_ID, pref.getString(KEY_ID, null));

        // retail price
        user.put(KEY_PRICE, pref.getString(KEY_PRICE, null));

        // return user
        return user;
    }

}
