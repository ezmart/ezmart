package donah.com.ezmart;

public interface TotalListener {
   public float onTotalChanged(float sum);
}
