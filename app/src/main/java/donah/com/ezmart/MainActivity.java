package donah.com.ezmart;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.app.ProgressDialog;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView recyclerView;
    private RecyclerViewMainAdapter mAdapter;
    Button totalPrice;
    private TrackGPS gps;
    double longitude;
    double latitude;
    LayoutInflater layoutInflater;
    private ProgressDialog pDialog;
    SharedPrefActivity sharedPrefActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // sharedPrefActivity = new SharedPrefActivity(getApplicationContext());

        totalPrice = (Button) findViewById(R.id.btnTotal);
        //Make call to AsyncTask
        new AsyncFetch().execute();
        turnGPSOn();

        totalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<itemModel> data = ((RecyclerViewMainAdapter) mAdapter).getPrice();

                float sum = 0;
                for (int i = 0; i < data.size(); i++) {
                    itemModel priceSelect = data.get(i);
                    if (priceSelect.isSelected() == true) {

                        //  data = data + "\n" + singleStudent.getName().toString();
                        sum += Float.parseFloat(priceSelect.retail);

                    }

                }
                AlertDialog.Builder build = new AlertDialog.Builder(MainActivity.this);
                build.setMessage("Your total: P" + Float.toString(sum)); // Want to enable?
                build.setIcon(R.drawable.ezmart_icon);
                build.setTitle("Total");
                build.setNegativeButton("OK", null);
                build.create().show();
                //.makeText(v.getContext(), Float.toString(sum), Toast.LENGTH_LONG).show();
            }
        });



    }

    public void turnGPSOn(){

        try
        {

            String provider = Settings.Secure.getString
                    (MainActivity.this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(!provider.contains("gps")){ //if gps is disabled
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("GPS is disabled in your device. Would you like to enable it?"); // Want to enable?
                builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.create().show();
                return;
            }
        }
        catch (Exception e) {

        }

    }

    public void turnGPSOff(){
        String provider = Settings.Secure.getString(MainActivity.this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(provider.contains("gps")){ //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            MainActivity.this.sendBroadcast(poke);
        }
    }

    // turning off the GPS if its in on state. to avoid the battery drain.
    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        turnGPSOff();
    }



    private class AsyncFetch extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your json file resides
                // Even you can make call to php file which returns json data
                url = new URL("http://diseasefindr.com/donah/records.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return e.toString();
            }
            try {

                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("GET");

                // setDoOutput to true as we recieve data from json file
                conn.setDoOutput(true);

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return e1.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {


            pdLoading.dismiss();
            List<itemModel> data=new ArrayList<>();

            pdLoading.dismiss();
            ConnectivityManager manager = (ConnectivityManager) MainActivity.this.getSystemService(MainActivity.this.CONNECTIVITY_SERVICE);
            NetworkInfo nf = manager.getActiveNetworkInfo();

            if (nf == null) {
                Toast.makeText(
                        MainActivity.this,
                        "No internet connection",
                        Toast.LENGTH_LONG).show();
            } else {
                try {

                    JSONArray jArray = new JSONArray(result);

                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        itemModel itemData = new itemModel();
                        itemData.description = json_data.getString("description");
                        itemData.retail = json_data.getString("retail");
                        itemData.record_id = json_data.getString("id");
                        data.add(itemData);
                    }

                    // Setup and Handover data to recyclerview
                    recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                    mAdapter = new RecyclerViewMainAdapter(MainActivity.this, data);
                    recyclerView.setAdapter(mAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.search_menu, menu);
        getMenuInflater().inflate(R.menu.check_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.search:   //this item has your app icon

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
               // View v = layoutInflater.inflate(R.layout.activity_search_dialog, null);
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                View v= inflater.inflate(R.layout.activity_search_dialog, null, false);

                final EditText search =(EditText) v.findViewById(R.id.search_item);
                builder.setIcon(R.drawable.ezmart_icon);
                builder.setTitle("Search");
                builder.setView(v);

                builder.setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("STRING", "search");

                        ConnectivityManager manager = (ConnectivityManager) MainActivity.this.getSystemService(MainActivity.this.CONNECTIVITY_SERVICE);
                        NetworkInfo nf = manager.getActiveNetworkInfo();

                        if (nf == null) {
                            Toast.makeText(
                                    MainActivity.this,
                                    "No internet connection",
                                    Toast.LENGTH_LONG).show();
                        } else {

                           // pDialog.setMessage("\tLoading...");
                          //  pDialog.setCancelable(false);
                          //  pDialog.show();

                            String searchItem = search.getText().toString();
                            Toast.makeText(MainActivity.this, searchItem, Toast.LENGTH_SHORT).show();

                            new getSearch().execute(searchItem);

                            Log.e("STRING", "search" + searchItem);
                        }
                    }
                });
                builder.create().show();

                return true;
            case R.id.checkSubmit:
             gps = new TrackGPS(MainActivity.this);


                if(gps.canGetLocation()){


                    longitude = gps.getLongitude();
                    latitude = gps .getLatitude();

                    Toast.makeText(getApplicationContext(),"Longitude:"+Double.toString(longitude)+"\nLatitude:"+Double.toString(latitude),Toast.LENGTH_SHORT).show();

                    CalculationByDistance(latitude,longitude);



                }
                else
                {
                    turnGPSOn();

                }


              // Intent intent = new Intent(MainActivity.this, MapsActivity.class);
              // startActivity(intent);

            default: return super.onOptionsItemSelected(item);
        }
    }

    public double CalculationByDistance(double lat, double lon) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = 0;
        double lon1 = 0;
        double lat2 = lat;
        double lon2 = lon;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
       /* Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);*/
        Toast.makeText(MainActivity.this, "Radius Value" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec, Toast.LENGTH_LONG).show();

        if (meterInDec > 25){
            AlertDialog.Builder build = new AlertDialog.Builder(MainActivity.this);
            build.setMessage("Your location is not near the NCCC Uyanguren store. You may only access this once you are near the store."); // Want to enable?
            build.setIcon(R.drawable.ezmart_icon);
            build.setTitle("Alert");
            build.setNegativeButton("OK", null);
            build.create().show();
        }else {
            Intent intent = new Intent(MainActivity.this, MapActivity.class);
            startActivity(intent);

        }

        return Radius * c;
    }


    private class getSearch extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection conn;
        URL url = null;
        String searchQuery;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String strUrl = "http://diseasefindr.com/donah/search_item.php?" +
                    "search_item="+params[0];

            URL url = null;
            StringBuffer sb = new StringBuffer();
            try {
                url = new URL(strUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream iStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
                String line = "";
                while( (line = reader.readLine()) != null){
                    sb.append(line);
                }

                reader.close();
                iStream.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("STRING", "Result" + result);
            pdLoading.dismiss();


            pdLoading.dismiss();
            if (result.isEmpty()) {
                Toast.makeText(MainActivity.this, "No results!", Toast.LENGTH_SHORT).show();


            } else {

                List<itemModel> data = new ArrayList<>();
                try {

                    JSONArray jArray = new JSONArray(result);

                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        itemModel itemData = new itemModel();
                        itemData.description = json_data.getString("description");
                        itemData.retail = json_data.getString("retail");
                        itemData.record_id = json_data.getString("id");
                        data.add(itemData);
                    }

                    // Setup and Handover data to recyclerview
                    recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                    mAdapter = new RecyclerViewMainAdapter(MainActivity.this, data);
                    recyclerView.setAdapter(mAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                }
            }


        }

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        menu.findItem(R.id.search).setEnabled(true);

        return super.onPrepareOptionsMenu(menu);
    }


}


